const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
var mongoose = require('mongoose');
const Passenger = require('./models/user');
const PassengerTrack = require('./models/passengertracking');
const VehicleTracking = require('./models/vehicletracking');
var Vehicle = require('./models/vehicle');
var Driver = require('./models/driver');
const VehicleCategory = require('./models/vehiclecategory');
var Trip = require('./models/trip');
const app = express();
var mongoose = require('mongoose');
var config = require('./config');
var geolib = require('geolib');
var otp = require('./services/randomnum');

//################### geocoder settings ###################################
var NodeGeocoder = require('node-geocoder');

var options = {
    provider: 'google',

    httpAdapter: 'https',
    apiKey: 'AIzaSyBA7M0LHCtRo2aB8b0n0xZOBL68lfCBt2A',
    formatter: null
};

var geocoder = NodeGeocoder(options);

//##########################################################################

app.get('/', (req, res) => {
    res.send("Socket server");
});

var cors = require('cors');
app.use(cors());


const server = http.Server(app);
const portSelected = config.PASSENGER_SOCKET_PORT;

server.listen(portSelected, function () {
    console.log('SNAP API driver Socket listening on port 8101');
});

var dbe = config.DB_URL;

mongoose.Promise = require('bluebird');
mongoose.connect(dbe, {
    useMongoClient: true
});

const io = socketIo(server);

io.on('connection', (socket) => {
    console.log('connection: ' + socket.id);
    socket.on('passengerConnected', async (res) => {
        console.log('passengerConnected')
        console.log(res)
        PassengerTrack.findOneAndRemove({
                'passengerId': res.passengerId
            })
            .exec(function (err, result) {
                var passengertrack = new PassengerTrack();
                passengertrack.passengerId = res.passengerId;
                passengertrack.socketId = socket.id;
                passengertrack.currentLocation.address = res.address;
                passengertrack.currentLocation.longitude = res.longitude;
                passengertrack.currentLocation.latitude = res.latitude;
                passengertrack.currentStatus = res.currentStatus;

                passengertrack.save(function (err, results) {
                    if (err) {
                        console.log('inPtrackErr: ' + err)
                        //io.to(socket.id).emit('passengerConnectResult', 'faild');
                    } else {
                        console.log('PtrackSave: ' + results)
                        io.to(results.socketId).emit('passengerConnectResult', results);
                    }
                })
            })
    });

    socket.on('disconnect', () => {
        console.log('disconnecting passenger');
        PassengerTrack.findOneAndRemove({
                'socketId': socket.id
            })
            .exec(function (err, result) {
                if (err) {
                    console.log('err')
                } else {
                    console.log('passenger track deleted.')
                }
            })
    })

    socket.on('toDisconnect', (data) => {
        console.log('disconnect by passenger');
        PassengerTrack.findOneAndRemove({
                $or: [{
                    'socketId': data.socketId
                }, {
                    'passengerId': data.passengerId
                }]
            })
            .exec(function (err, result) {
                if (err) {
                    console.log('err')
                } else {
                    console.log('passenger track deleted.')
                }
            })
    })

    socket.on('recon', (data) => {
        console.log('reconnected');
        var newVals = {
            $set: {
                'socketId': socket.id
            }
        }

        PassengerTrack.findOneAndUpdate({
            'passengerId': data.passengerId
        }, newVals, function (err, results) {
            if (err) {
                console.log('err in reconnecting')
                //io.to(socket.id).emit('reConnectResult', 'faild!');
            } else {
                if (results != null) {
                    // console.log('reconnect success!')
                    // console.log(results);
                    io.to(socket.id).emit('reConnectResult', results);
                } else {
                    if (data.passengerId && data.currentStatus) {
                        var passengertrack = new PassengerTrack();
                        passengertrack.passengerId = data.passengerId;
                        passengertrack.socketId = socket.id;
                        passengertrack.currentLocation.address = data.address;
                        passengertrack.currentLocation.longitude = data.longitude;
                        passengertrack.currentLocation.latitude = data.latitude;
                        passengertrack.currentStatus = data.currentStatus;

                        passengertrack.save(function (err, results) {
                            if (err) {
                                console.log('inPtrackErr: ' + err)
                            } else {
                                console.log('PassengertrackSave')
                                io.to(results.socketId).emit('passengerConnectResult', results);
                            }
                        })
                    } else {
                        console.log('inside else');
                        io.to(socket.id).emit('reConnectResult', results);
                    }

                }
            }
        })
    })

    socket.on('getOnlineDriversBylocation', (data1) => {
        VehicleTracking.find({
                currentStatus: 'online'
            })
            .exec(function (err, data) {
                if (err) {
                    console.log('ERRR')
                } else {
                    if (data.length === 0) {
                        console.log("####################### No online Drivers ##########################");
                    } else {
                        //   console.log(data)
                        var tempDrivers = [];
                        if(data.radius) {
                            var radius = parseInt(data1.radius * 1000);
                        } else {
                            var radius = 5000;
                        }

                        data.forEach(function (element) {

                            if (
                                geolib.isPointInCircle({
                                        latitude: element.currentLocation.latitude,
                                        longitude: element.currentLocation.longitude
                                    }, {
                                        latitude: data1.latitude,
                                        longitude: data1.longitude
                                    },
                                    radius
                                )
                            ) {
                                tempDrivers.push(element);
                            }
                        });

                        let resp = [];

                        tempDrivers.forEach((el,i) => {
                            resp.push({
                                currentLocation: {
                                    address : el.currentLocation.address,
                                    latitude : el.currentLocation.latitude,
                                    longitude : el.currentLocation.longitude,
                                },
                                currentStatus: el.currentStatus,
                                driverId: el.driverId,
                                driverInfo: el.driverInfo,
                                vehicleCategory: el.vehicleCategory,
                                vehicleCategoryData: el.vehicleCategoryData,
                                vehicleInfo: el.vehicleInfo,
                                vehicleSubCategory: el.vehicleSubCategory
                            });
                            resp.push({
                                currentLocation: {
                                    address : el.currentLocation.address,
                                    latitude : 2 * data1.latitude - el.currentLocation.latitude,
                                    longitude : (i % 2 == 0) ? data1.longitude + (0.001 * (5 + i) % 8)  :data1.longitude + (0.002 * (7 + i) % 8),
                                },
                                currentStatus: el.currentStatus,
                                driverId: el.driverId + i + '-1',
                                driverInfo: el.driverInfo,
                                vehicleCategory: el.vehicleCategory,
                                vehicleCategoryData: el.vehicleCategoryData,
                                vehicleInfo: el.vehicleInfo,
                                vehicleSubCategory: el.vehicleSubCategory
                            });
                            resp.push({
                                currentLocation: {
                                    address : el.currentLocation.address,
                                    latitude : (i % 2 == 0) ? data1.latitude + (0.001 * (6 + i) % 8) : data1.latitude + (0.002 * (5 + i) % 8),
                                    longitude : (i % 2 == 0) ? data1.longitude + (0.002 * (7 + i) % 9) : data1.longitude + (0.001 * (4 + i) % 9),
                                },
                                currentStatus: el.currentStatus,
                                driverId: el.driverId + i + '-2',
                                driverInfo: el.driverInfo,
                                vehicleCategory: el.vehicleCategory,
                                vehicleCategoryData: el.vehicleCategoryData,
                                vehicleInfo: el.vehicleInfo,
                                vehicleSubCategory: el.vehicleSubCategory
                            });
                            // resp.push({
                            //     currentLocation: {
                            //         address : el.currentLocation.address,
                            //         latitude : (i % 2 == 0) ? el.currentLocation.latitude + (0.001 * (6 + i) % 9) : el.currentLocation.latitude + (0.001 * (3 + i) % 9),
                            //         longitude : (i % 2 == 0) ? el.currentLocation.longitude + (0.001 * (4 + i) % 8) : el.currentLocation.longitude + (0.001 * (6 + i) % 8),
                            //     },
                            //     currentStatus: el.currentStatus,
                            //     driverId: el.driverId + i + '-3',
                            //     driverInfo: el.driverInfo,
                            //     vehicleCategory: el.vehicleCategory,
                            //     vehicleCategoryData: el.vehicleCategoryData,
                            //     vehicleInfo: el.vehicleInfo,
                            //     vehicleSubCategory: el.vehicleSubCategory
                            // });
                        });

                        io.to(data1.socketId).emit('allOnlineDriversResult', resp);
                    }
                }
            });

    });

    socket.on('TripDetails', (data) => {
        // console.log(data);
        io.to(data.socketId).volatile.emit('driverDetails', data)
    })

    socket.on('getDriverLocationById', (data) => {
        VehicleTracking.findOne({
                driverId: data.driverId
            })
            .exec(function (err, driverTrack) {
                if (err) {
                    console.log('####Error in vehicle tracking');
                } else {
                    if (driverTrack == null) {
                        console.log()
                    } else {
                        // console.log(driverTrack)
                        io.to(data.socketId).emit('getDriverLocationByIdResult', driverTrack)

                        if(data.tripId) {
                            Trip.findById(mongoose.Types.ObjectId(data.tripId), function (err, tripData) {
                                if (tripData.status == 'canceled') {
                                    var passengerSocketObj = {
                                        socketId: data.socketId,
                                        canceledDriverId: tripData.cancelDetails[0].canceledDriverId,
                                        cancelReason: tripData.cancelDetails[0].cancelReason
                                      }
                                      io.to(data.socketId).emit('tripCancelByDriver', passengerSocketObj)  
                                }
                            })
                        } 
                    }
                }
            })
    })

    socket.on('TripCancel', (data) => {
        console.log(data);
        io.to(data.socketId).emit('tripCancelByDriver', data)
    })

    socket.on('EndTrip', (data) => {
        console.log(data);
        io.to(data.socketId).emit('tripEndByDriver', data)
    })

    socket.on('getTripAcceptDetails', (data) => {
        //req.body.tripId
        //req.body.socketId
        Trip.findById(mongoose.Types.ObjectId(data.tripId), function (err, tripData) {
            if (err) {
                console.log('################### error finding trip ####################');
            } else {
                if (tripData.status == 'accepted' && tripData.assignedDriverId != null) {
                    VehicleTracking.findOne({
                        driverId: tripData.assignedDriverId
                    }, function (err1, driverData) {
                        if (err1) {
                            console.log('################### error finding driver ####################');
                        } else {
                            Vehicle.findById(tripData.assignedVehicleId, function (err2, vehicleData) {
                                if (err2) {
                                    console.log('################### error finding vehicle ####################');
                                } else {
                                    if (data.socketId) {
                                        var tripDetailsObj = {
                                            tripId: tripData._id,
                                            driverId: driverData.driverId,
                                            driverName: driverData.driverInfo.driverName,
                                            driverContactNo: driverData.driverInfo.driverContactNumber,
                                            driverPic: driverData.driverPic,
                                            vehicleId: vehicleData._id,
                                            vehicleRegistrationNo: vehicleData.vehicleRegistrationNo,
                                            vehicleBrand: vehicleData.vehicleBrandName,
                                            vehicleModel: vehicleData.vehicleModel,
                                            vehicleColor: vehicleData.vehicleColor,
                                            longitude: driverData.currentLocation.longitude,
                                            latitude: driverData.currentLocation.latitude,
                                            socketId: data.socketId
                                        }

                                        io.to(data.socketId).volatile.emit('driverDetails', tripDetailsObj)

                                    }
                                }
                            })
                        }
                    })
                }
            }

        })
    });


    socket.on('getTripEndtDetails', (data) => {
        //req.body.tripId
        //req.body.socketId
        Trip.findById(mongoose.Types.ObjectId(data.tripId), function (err, tripData) {
            if (err) {
                console.log('################### error finding trip ####################');
            } else {
                if (tripData.status == 'done' && tripData.assignedDriverId != null) {
                    if (data.socketId) {
                        var passengerSocketObj = {
                            distance: tripData.distance,
                            totalPrice: tripData.totalPrice,
                            waitTime: tripData.waitTime,
                            waitingCost: tripData.waitingCost,
                            discount: 0,
                            dropDateTime: tripData.dropDateTime,
                            tripTime: tripData.tripTime,
                            socketId: data.socketId
                          }
                        io.to(data.socketId).volatile.emit('tripEndByDriver', passengerSocketObj)
                    }
                }
            }
        });
    });





});