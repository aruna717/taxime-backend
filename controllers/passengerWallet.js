'use strict';


var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var PassengerWallet = require('../models/passengerwallet');

app.use(cors())
router.use(cors())


//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

exports.PassengerWallet = function (req, res) {
    console.log("###### Admin ######");
    res.json({
        status: 'passenger'
    });
};

//create a wallet for passenger
exports.createWallet = function (req, res) {
    console.log(req.body);
    if (req.body.passengerId) {
        var wallet = new PassengerWallet();
        wallet.passengerId = req.body.passengerId;

        wallet.save(function (err, wallet) {
            if (err) {
                return res.status(500).send(err);
            } else {
                return res.status(200).json({message: 'success!'});
            }
        })
    } else {
        return res.status(500).json({message: 'Server Error!'});
    }
}

//recharge wallet amount of passenger
exports.rechargeWallet = function (req, res) {
    var newVals = {
        $inc: {
            totalWalletPoints : req.body.rechargeAmount
        }
    }
    PassengerWallet.findOneAndUpdate({passengerId: req.body.passengerId}, newVals, function (err, wallet) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({message: wallet});
        }
    });
}

//update wallet amount of passenger
exports.updateWallet = function(req, res) {
    var newVals = {
        $set : {
            totalWalletPoints : req.body.rechargeAmount
        }
    }
    PassengerWallet.findOneAndUpdate({passengerId: req.body.passengerId}, newVals, function (err, wallet) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({message: wallet});
        }
    });

}

//get wallet details of passenger
exports.getWallet = function (req, res) {
    PassengerWallet.find({driverId: req.body.passengerId}, function (err, wallet) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success', details: "get data Successfully", content: wallet });
        }
    });
}
