'use strict';


var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var Admin = require('../models/admin');
var Driver = require('../models/driver');
var Dispatcher = require('../models/dispatcher');
const Vehicle = require('../models/vehicle');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var imageUpload = require('./imageUpload');
var otp = require('../services/randomnum');
var adminRegEmail = require('../emailTemplate/adminRegister');
var User = require('../models/user');
var VehicleTracking = require('../models/vehicletracking');
var VehicleCategory = require('../models/vehiclecategory');
var geolib = require('geolib');
var ManualCustomer = require('../models/manualcustomer');
var CompanyWallet = require('../models/companywallet');
var RoadPickup = require('../models/roadPickupTrip')
var Trip = require('../models/trip');
var Settings = require('../models/setting');

var Dispatch = require('../models/dispatch');
// var Vehicle = require('../models/vehicle');

app.use(cors())
router.use(cors())


//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

exports.admin = function (req, res) {
    console.log("###### Admin ######");
    res.json({
        status: 'Admin'
    });
};

exports.registerAdmin = function (req, res) {
    console.log("###### admin register ######");
    Admin.findOne({
        'email': req.body.email
    })
        .exec(function (err, admins) {
            if (err) {
                console.log('####### error occured');
            } else {
                if (admins !== null) {
                    console.log("####################### not an null data : user already exist ##########################");
                    res.status(409).json({
                        message: 'email taken'
                    });
                } else {
                    console.log("####################### null data ##########################");
                    // console.log(req);
                    var admin = new Admin();
                    admin.firstName = req.body.firstName;
                    admin.lastName = req.body.lastName;
                    admin.email = req.body.email;
                    admin.nic = req.body.nic;
                    admin.birthday = req.body.birthday;
                    admin.password = bcrypt.hashSync(req.body.password, 10);
                    admin.mobile = req.body.mobile;
                    admin.gender = req.body.gender;
                    admin.role = req.body.role;
                    admin.address.address = req.body.address;
                    admin.address.street = req.body.street;
                    admin.address.city = req.body.city;
                    admin.address.zipcode = req.body.zipcode;
                    admin.address.country = req.body.country;

                    admin.save(function (err) {
                        if (err) {
                            console.log('#################### error occured #######################');
                            console.log(err);
                            res.send(err);
                        } else {
                            res.status(200).json({
                                message: 'success',
                                details: "Signup successful"
                            });
                        }
                    });
                }
            }
        });
};

/* ### SignIn / Login ### */
exports.signInAdmin = function (req, res) {
    console.log("###### admin signIn #######");
    //res.json({status: 'sign In'});
    Admin.findOne({
        'email': req.body.email
    })
        .exec(function (err, admin) {
            if (err) {
                console.log('####### error occured' + err);
                // logger.error(err)
                res.send('error');
            } else {
                if (admin !== null) {
                    console.log("####################### not an null data : admin already exist ##########################");

                    if (admin.isVerified) {

                        if (bcrypt.compareSync(req.body.password, admin.password)) {
                            let admi = {
                                id: admin._id,
                                name: admin.firstName + ' ' + admin.lastName,
                                role: admin.role
                            }
                            res.status(200).json({
                                message: 'success',
                                details: "Login successfully",
                                content: admi,
                                token: jwt.sign({
                                    email: admin.email,
                                    firstName: admin.firstName,
                                    lastName: admin.lastName,
                                    role: admin.role,
                                    _id: admin._id
                                }, 'RESTFULAPIs')
                            });
                        } else {
                            res.status(400).json({
                                message: 'failed',
                                details: "Username or Password Incorrect",
                                status: "signin_failed"
                            });
                        }
                    } else {
                        res.status(400).json({
                            message: 'failed',
                            details: "Please confirm your Email",
                            status: "signin_failed"
                        });
                    }
                } else {
                    console.log("####################### null data ##########################");
                    res.status(403).json({
                        message: 'failed',
                        details: "Username or Password Incorrect",
                        status: "signin_failed"
                    });
                }
            }
        });
};

// Admin add Driver

exports.addDriver = function (req, res) {
    console.log('Add driver by admin');
    if (req.files.driverPic && req.files.nicFrontPic && req.files.nicBackPic &&
        req.files.drivingLicenceFrontPic && req.files.drivingLicenceBackPic) {
        Driver.findOne({
            $or: [{
                'email': req.body.email
            }, {
                'mobile': req.body.mobile
            }]
        })
            .exec(function (err, drivers) {
                if (err) {
                    console.log('####### error occured' + err);
                    res.status(500).json({
                        message: 'internel error'
                    });
                } else {
                    if (drivers !== null) {
                        console.log("####################### not an null data : Driver already exist ##########################");
                        res.status(409).json({
                            message: 'failed',
                            details: "email or mobile already registered!",
                            status: "signup_failed"
                        });
                    } else {
                        console.log("####################### null data ##########################");
                        var driver = new Driver();
                        driver.firstName = req.body.firstName;
                        driver.lastName = req.body.lastName;
                        driver.email = req.body.email;
                        driver.otpPin = otp.otpGen();
                        driver.otpTime = new Date();
                        driver.nic = req.body.nic;
                        driver.birthday = req.body.birthday;
                        driver.mobile = req.body.mobile;
                        driver.gender = req.body.gender;
                        driver.address.address = req.body.address;
                        driver.address.street = req.body.street;
                        driver.address.city = req.body.city;
                        driver.address.zipcode = req.body.zipcode;
                        driver.address.country = req.body.country;
                        driver.lifeInsuranceNo = req.body.lifeInsuranceNo;
                        driver.lifeInsuranceExpiryDate = req.body.lifeInsuranceExpiryDate;
                        driver.lifeInsuranceAmount = req.body.lifeInsuranceAmount;
                        driver.isApproved = true;
                        let salt = bcrypt.hashSync(req.body.email, 5);
                        driver.saltSecret = salt.replace(/[^a-zA-Z ]/g, "");


                        driver.save(function (err) {
                            if (err) {
                                console.log('#################### error occured #######################');
                                console.log(err);
                                res.send(err);
                            } else {
                                imageUpload.uploadImages(req.files, driver._id, Driver, 'drivers');
                                // driverRegEmail.driverRegEmail(req.body.email, driver.saltSecret);
                                res.status(200).json({
                                    message: 'success',
                                    details: "Driver registered successfully"
                                });
                            }
                        });

                    }
                }
            });
    } else {
        res.status(409).json({
            message: 'driver images are missing'
        });
    }

}

/// admin get driver details ///
exports.getDrivers = function (req, res) {
    Driver.find({}).select({
        "otpPin": 0,
        "otpTime": 0,
        "saltSecret": 0
    })
        .exec(function (err, drivers) {
            if (err) {
                res.status(500).json({
                    message: 'server error'
                });
            } else {
                if (drivers !== null) {
                    res.status(200).json({
                        message: 'selection success',
                        content: drivers
                    });
                } else {
                    res.status(404).json({
                        message: 'no drivers found'
                    });
                }
            }

        })
}

// admin approve driver //
exports.adminApproveDriver = function (req, res) {
    console.log('### in approve ###');
    Driver.findOne({
        '_id': req.body.id
    })
        .exec(function (err, driver) {
            if (err) {
                res.status(500).json({
                    message: 'internel error'
                });
            } else {
                if (driver !== null) {
                    if (driver.isApproved == false) {
                        var newValues = {
                            $set: {
                                isApproved: true
                            }
                        }
                        Driver.findByIdAndUpdate(req.body.id, newValues, function (error, results) {
                            if (error) {
                                res.status(500).json({
                                    message: 'server error'
                                });
                            } else {
                                res.status(200).json({
                                    message: 'Driver Approve successfully!'
                                });
                            }
                        })
                    } else {
                        res.status(409).json({
                            message: 'Driver already approved'
                        });
                    }

                } else {
                    res.status(404).json({
                        message: 'Driver not found'
                    });
                }
            }
        })
}

// admin get drivers to approve //
exports.getDriversToApprove = function (req, res) {
    Driver.find({
        'isApproved': false
    }).select({
        "otpPin": 0,
        "otpTime": 0,
        "saltSecret": 0
    })
        .exec(function (err, drivers) {
            if (err) {
                res.status(500).json({
                    message: 'internel error'
                });
            } else {
                if (drivers.length > 0) {
                    res.status(200).json({
                        message: 'success',
                        content: drivers
                    });
                } else {
                    res.status(404).json({
                        message: 'No drivers to approve'
                    });
                }
            }
        })
}

/**get users */
exports.getAllUsers = function (req, res) {

    console.log("###### get Users ######");
    User.find({ isApproved: true })
        .exec(function (err, category) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                if (category == null) {
                    res.status(400).json({
                        message: 'failed',
                        details: "No data found",
                        status: "failed"
                    });
                } else {
                    res.json({
                        message: 'success',
                        details: "get User data Successfully",
                        content: category
                    });
                }
            }
        });

};

/**get users */
exports.getAllUserspagination = function (req, res) {

    var pageNo = req.body.pageNo;
    var paginationCount = req.body.paginationCount;
    var responseData;
    
    var param = req.body.param;

    console.log("###### get Users ######");
    // User.find({ isApproved: true, [param]: { $regex: req.body.text } })
    User.aggregate([{
        $match: {
            $and: [
                {
                    isApproved: true
                }
                // {
                //     [param]: { $regex: req.body.text }
                // }
            ],
        },
    },
    {
        $lookup: {
            from: "passengerwallets",
            localField: "_id",
            foreignField: "passengerId",
            as: "wallet"
        }
    },
    {
        $match: {
            $and: [
                {
                    [param]: { $regex: req.body.text }
                }
            ],
        },
    },
    ])
        .sort({ recordedTime: -1 })
        .exec(function (err, data) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                if (data == null) {
                    res.status(400).json({
                        message: 'failed',
                        details: "No data found",
                        status: "failed"
                    });
                } else {
                    responseData = data.slice(((pageNo - 1) * paginationCount), (pageNo * paginationCount));

                    res.status(200).json({
                        message: 'Success!',
                        content: responseData,
                        noOfPages: data / paginationCount,
                        noOfRecords: data.length
                    });

                    // res.json({
                    //     message: 'success',
                    //     details: "get User data Successfully",
                    //     content: data
                    // });
                }
            }
        });

};

exports.addUser = function (req, res) {

    var user = new User();
    user.name = req.body.firstName + " " + req.body.lastName;
    user.email = null;
    user.birthday = req.body.birthday;
    user.contactNumber = req.body.mobile;
    user.address.address = req.body.address;
    user.address.zipcode = req.body.zipcode;
    user.address.city = req.body.city;
    user.address.country = req.body.country;
    user.isApproved = true;

    user.save(function (err, result) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success!' });
        }
    });
}

exports.clearVehicleTracks = function (req, res) {
    VehicleTracking.remove({})
        .exec(function (err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).json({ message: 'success!' });
            }
        })
}

exports.adminloginRequired = function (req, res, next) {
    console.log("###### login required ######");
    // console.log(role)
    // return function(req, res, next) {
    if (req.user) {
        console.log('##$$%##')
        console.log(req.user)

        Admin.findOne({
            '_id': req.user._id
        })
            .exec(function (err, admin) {
                if (err) {
                    console.log('err');
                    res.status(401).json({
                        message: 'Unauthorized user!'
                    });
                } else {
                    if (admin !== null) {
                        // if(arguments.length > 0) {
                        //     var allow = false;
                        //     for (var i=0; i < arguments.length; i++) {

                        //         if(arguments[i] == admin.role) {

                        //             allow = true;
                        //             break;
                        //         }
                        //     }
                        //     if(allow) {
                        //         next();
                        //     } else {
                        //         res.status(401).json({
                        //             message: 'Unauthorized user!'
                        //         });
                        //     }
                        // } 
                        // else {
                        //     next();
                        // }
                        next();
                    } else {
                        res.status(401).json({
                            message: 'Unauthorized user!'
                        });
                    }
                }
            })
    } else {
        res.status(401).json({
            message: 'Unauthorized user!'
        });
    }
    // };

};

// get all drivers in vehicle tracking
exports.getVehicleTracking = function (req, res) {

    VehicleTracking.aggregate([{
        $lookup: {
            from: "vehiclecategories",
            localField: "vehicleCategory",
            foreignField: "categoryName",
            as: "vehicleCategoryData"
        }
    },
    {
        "$unwind": "$vehicleCategoryData"
    },
    // { "$match": { "vehicleCategory.subCategory.subCategoryName": "test1" } },      
    {
        $project: {
            "_id": 1,
            "currentStatus": 1,
            "vehicleSubCategory": 1,
            "vehicleCategory": 1,
            "driverId": 1,
            "currentLocation": 1,
            "driverInfo": 1,
            "vehicleInfo": 1,
            "vehicleCategoryData.subCategory": {
                $filter: {
                    input: "$vehicleCategoryData.subCategory",
                    as: "subCat",
                    cond: {
                        $eq: ["$$subCat.subCategoryName", "$vehicleSubCategory"]
                    }
                }
            }
        }
    }

    ])
        .exec(function (err, data) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                if (data == null) {
                    res.status(400).json({
                        message: 'failed',
                        details: "No data found",
                        status: "failed"
                    });
                } else {

                    let resp = [];
/*
                    data.forEach((el,i) => {
                        resp.push({
                            currentLocation: {
                                address : el.currentLocation.address,
                                latitude : el.currentLocation.latitude,
                                longitude : el.currentLocation.longitude,
                            },
                            currentStatus: el.currentStatus,
                            driverId: el.driverId,
                            driverInfo: el.driverInfo,
                            vehicleCategory: el.vehicleCategory,
                            vehicleCategoryData: el.vehicleCategoryData,
                            vehicleInfo: el.vehicleInfo,
                            vehicleSubCategory: el.vehicleSubCategory
                        });
                        resp.push({
                            currentLocation: {
                                address : el.currentLocation.address,
                                latitude : el.currentLocation.latitude + (0.001 * otp.rndNo()),
                                longitude : el.currentLocation.longitude + (0.001 * otp.rndNo()),
                            },
                            currentStatus: el.currentStatus,
                            driverId: el.driverId + i + '-1',
                            driverInfo: el.driverInfo,
                            vehicleCategory: el.vehicleCategory,
                            vehicleCategoryData: el.vehicleCategoryData,
                            vehicleInfo: el.vehicleInfo,
                            vehicleSubCategory: el.vehicleSubCategory
                        });
                        resp.push({
                            currentLocation: {
                                address : el.currentLocation.address,
                                latitude : el.currentLocation.latitude - (0.001 * otp.rndNo()),
                                longitude : el.currentLocation.longitude - (0.001 * otp.rndNo()),
                            },
                            currentStatus: el.currentStatus,
                            driverId: el.driverId + i + '-2',
                            driverInfo: el.driverInfo,
                            vehicleCategory: el.vehicleCategory,
                            vehicleCategoryData: el.vehicleCategoryData,
                            vehicleInfo: el.vehicleInfo,
                            vehicleSubCategory: el.vehicleSubCategory
                        });
                        resp.push({
                            currentLocation: {
                                address : el.currentLocation.address,
                                latitude : el.currentLocation.latitude - (0.001 * otp.rndNo()),
                                longitude : el.currentLocation.longitude - (0.001 * otp.rndNo()),
                            },
                            currentStatus: el.currentStatus,
                            driverId: el.driverId + i + '-3',
                            driverInfo: el.driverInfo,
                            vehicleCategory: el.vehicleCategory,
                            vehicleCategoryData: el.vehicleCategoryData,
                            vehicleInfo: el.vehicleInfo,
                            vehicleSubCategory: el.vehicleSubCategory
                        });
                    });
*/
                    res.json({
                        message: 'success',
                        details: "get data Successfully",
                        content: data
                    });
                }
            }
        });
}

// get online drivers near pickup location
exports.getOnlneDriversByRadious = function (req, res) {
    VehicleTracking.aggregate([{
        $match: {
            'currentStatus': 'online'
        }
    },
    {
        $lookup: {
            from: "vehiclecategories",
            localField: "vehicleCategory",
            foreignField: "categoryName",
            as: "vehicleCategoryData"
        }
    },
    {
        "$unwind": "$vehicleCategoryData"
    },
    // { "$match": { "vehicleCategory.subCategory.subCategoryName": "test1" } },      
    {
        $project: {
            "_id": 1,
            "currentStatus": 1,
            "vehicleSubCategory": 1,
            "vehicleCategory": 1,
            "driverId": 1,
            "currentLocation": 1,
            "driverInfo": 1,
            "vehicleInfo": 1,
            "vehicleCategoryData.subCategory": {
                $filter: {
                    input: "$vehicleCategoryData.subCategory",
                    as: "subCat",
                    cond: {
                        $eq: ["$$subCat.subCategoryName", "$vehicleSubCategory"]
                    }
                }
            }
        }
    }
    ])
        .exec(function (err, data) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                if (data == null) {
                    res.status(400).json({
                        message: 'failed',
                        details: "No data found",
                        status: "failed"
                    });
                } else {

                    var tempData = [];

                    data.forEach(function (element) {

                        if (
                            geolib.isPointInCircle({
                                latitude: element.currentLocation.latitude,
                                longitude: element.currentLocation.longitude
                            }, {
                                    latitude: req.body.latitude,
                                    longitude: req.body.longitude
                                },
                                5000
                            )
                        ) {

                            tempData.push(element);
                        }
                    });

                    res.json({
                        message: 'success',
                        details: "get category data Successfully",
                        content: tempData
                    });
                }
            }
        });
}


/**get Manual customers */
exports.getAllManualCustomers = function (req, res) {

    console.log("###### get Manual customers ######");

    console.log(req.params.text);

    ManualCustomer.find({ mobile: { $regex: req.params.text } })
        .exec(function (err, data) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                res.json({
                    message: 'success',
                    details: "get data Successfully",
                    content: data
                });
            }
        });

};

/**get Manual customers */
exports.getAllManualCustomerspagination = function (req, res) {

    var pageNo = req.body.pageNo;
    var paginationCount = req.body.paginationCount;
    var responseData;
    var param = req.body.param;

    console.log("###### get Manual customers ######");

    console.log(req.params.text);

    ManualCustomer.find({ [param]: { $regex: req.body.text } })
        .sort({ recordedTime: -1 })
        .exec(function (err, data) {
            if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
            } else {
                responseData = data.slice(((pageNo - 1) * paginationCount), (pageNo * paginationCount));

                res.status(200).json({
                    message: 'Success!',
                    content: responseData,
                    noOfPages: data / paginationCount,
                    noOfRecords: data.length
                });
            }
        });

};


exports.createCompanyWallet = function (req, res) {

    var wallet = new CompanyWallet();
    wallet.companyName = 'SNAPCompany';
    wallet.save(function (err, walletres) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success' })
        }
    })
}

exports.getDashboardData = function (req, res) {

    var fromDate = new Date(req.params.from);
    var toDate = new Date(req.params.to);

    var i = 0;

    var response = {
        totalIncome: 0,
        commissionIncome: 0,
        CompletedTrips: 0,
        CanceledTrips: 0,
        RegisteredDriveres: 0,
        pendingDrivers: 0,
        registeredVehicles: 0,
        pendingVehicles: 0
    }

    //completed trips
    // Dispatch.aggregate([{
    //     $match: {
    //         $and: [{
    //             $or: [{
    //                 status: 'done'
    //             }]
    //         },
    //         {
    //             recordedTime: {
    //                 $gte: fromDate,
    //                 $lt: toDate
    //             }
    //         }
    //         ],
    //     },
    // }])
    //     .exec(function (err, result) {
    //         if (err) {
    //             // res.status(500).send(err)
    //             i++;
    //         } else {
    //             response.CompletedTrips = result.length;
    //             i++;
    //             if (i == 7) {
    //                 res.status(200).json({
    //                     message: 'success',
    //                     content: response
    //                 });
    //             }
    //         }
    //     })

    //canceled trips
    Dispatch.aggregate([{
        $match: {
            $and: [{
                $or: [{
                    status: 'canceled'
                }]
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }
            ],
        },
    }])
        .exec(function (err, result) {
            if (err) {
                // res.status(500).send(err)
                i++;
            } else {
                response.CanceledTrips = response.CanceledTrips + result.length;
                // response.CanceledTrips = result.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        })

    //canceled trips
    Trip.aggregate([{
        $match: {
            $and: [{
                $or: [{
                    status: 'canceled'
                }]
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }
            ],
        },
    }])
        .exec(function (err, result1) {
            if (err) {
                // res.status(500).send(err)
                i++;
            } else {
                response.CanceledTrips = response.CanceledTrips + result1.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        })

    //Approved drivres
    Driver.aggregate([{
        $match: {
            $and: [{
                'isApproved': true,
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }]
        },
    },
    ])
        .exec(function (err, drivers) {
            if (err) {
                // res.status(500).json({
                //     message: 'Internel error'
                // });
                i++;
            } else {
                response.RegisteredDriveres = drivers.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        });

    //pending drivres
    Driver.aggregate([{
        $match: {
            $and: [{
                'isApproved': false,
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }]
        },
    }])
        .exec(function (err, drivers) {
            if (err) {
                // res.status(500).json({
                //     message: 'Internel error'
                // });
                i++;
            } else {
                response.pendingDrivers = drivers.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        });

    // approved vehicles
    Vehicle.aggregate([{
        $match: {
            $and: [{
                'isApproved': true,
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }]
        },
    }])
        .exec(function (err, vehicles) {
            if (err) {
                // res.status(500).json({
                //     message: 'internel error'
                // });
                i++;
            } else {
                response.registeredVehicles = vehicles.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        })


    // pending vehicles
    Vehicle.aggregate([{
        $match: {
            $and: [{
                'isApproved': false,
            },
            {
                recordedTime: {
                    $gte: fromDate,
                    $lt: toDate
                }
            }]
        },
    }])
        .exec(function (err, vehicles) {
            if (err) {
                // res.status(500).json({
                //     message: 'internel error'
                // });
                i++;
            } else {
                response.pendingVehicles = vehicles.length;
                i++;
                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }
            }
        })

    CompanyWallet.aggregate(
        {
            "$unwind": "$transactionHistory"
        },
        {
            $match: {
                $and: [
                    {
                        'transactionHistory.dateTime': {
                            $gte: fromDate,
                            $lt: toDate
                        }
                    }
                ],
            },
        })
        .exec(function (err, result) {
            if (err) {
                i++;
            } else {
                i++;
                var totalEarningsCal = 0
                var commissionEarningsCal = 0
                var completedTripCal = 0;

                result.forEach(el => {
                    if (el.transactionHistory.trip != null) {
                        commissionEarningsCal = commissionEarningsCal + el.transactionHistory.trip.tripEarning;
                        
                        if (el.transactionHistory.isATrip) {
                            completedTripCal = completedTripCal + 1;
                            totalEarningsCal = totalEarningsCal + el.transactionHistory.trip.totalTripValue;
                        }
                        else{
                            totalEarningsCal = totalEarningsCal + el.transactionHistory.trip.tripEarning;
                        }
                    }
                });

                response.totalIncome = totalEarningsCal;
                response.commissionIncome = commissionEarningsCal;
                response.CompletedTrips = completedTripCal;

                if (i == 7) {
                    res.status(200).json({
                        message: 'success',
                        content: response
                    });
                }

            }
        })
}

exports.getCompanyWallet = function (req, res) {
    var fromDate = new Date(req.params.from);
    var toDate = new Date(req.params.to);

    CompanyWallet.aggregate(
        {
            "$unwind": "$transactionHistory"
        },
        {
            $lookup: {
                from: "dispatches",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataDispatch"
            }
        },
        {
            $lookup: {
                from: "roadpickuptrips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataRoadpickup"
            }
        },
        {
            $lookup: {
                from: "trips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataTrip"
            }
        },
        {
            $match: {
                $and: [
                    {
                        'transactionHistory.dateTime': {
                            $gte: fromDate,
                            $lt: toDate
                        }
                    }
                ],
            },
        })
        .exec(function (err, result) {
            if (err) {
                res.status(500).send(err)
            } else {

                var totalEarningsCal = 0

                result.forEach(el => {
                    if (el.transactionHistory.trip != null) {
                        totalEarningsCal = totalEarningsCal + el.transactionHistory.trip.tripEarning;
                    }
                });

                res.status(200).json({
                    companyWallet: result,
                    totalEarnings: totalEarningsCal,
                })

                // res.status(200).json({
                //     content: result
                // })
            }
        })
}

exports.getCompanyWalletpagination = function (req, res) {
    var fromDate = new Date(req.params.from);
    var toDate = new Date(req.params.to);

    CompanyWallet.aggregate(
        {
            "$unwind": "$transactionHistory"
        },
        {
            $lookup: {
                from: "dispatches",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataDispatch"
            }
        },
        {
            $lookup: {
                from: "roadpickuptrips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataRoadpickup"
            }
        },
        {
            $lookup: {
                from: "trips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataTrip"
            }
        },
        {
            $match: {
                $and: [
                    {
                        'transactionHistory.dateTime': {
                            $gte: fromDate,
                            $lt: toDate
                        }
                    }
                ],
            },
        })
        .exec(function (err, result) {
            if (err) {
                res.status(500).send(err)
            } else {

                var totalEarningsCal = 0

                result.forEach(el => {
                    if (el.transactionHistory.trip != null) {
                        totalEarningsCal = totalEarningsCal + el.transactionHistory.trip.tripEarning;
                    }
                });

                res.status(200).json({
                    companyWallet: result,
                    totalEarnings: totalEarningsCal,
                })

                // res.status(200).json({
                //     content: result
                // })
            }
        })
}

exports.getCompanyWalletpagination = function (req, res) {
    var fromDate = new Date(req.params.from);
    var toDate = new Date(req.params.to);

    var pageNo = req.params.pageNo;
    var paginationCount = 10;
    var responseData;
    var param = req.params.param;

    CompanyWallet.aggregate(
        {
            "$unwind": "$transactionHistory"
        },
        {
            $lookup: {
                from: "dispatches",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataDispatch"
            }
        },
        {
            $lookup: {
                from: "roadpickuptrips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataRoadpickup"
            }
        },
        {
            $lookup: {
                from: "trips",
                localField: "transactionHistory.trip.tripId",
                foreignField: "_id",
                as: "tripdataTrip"
            }
        },
        {
            $match: {
                $and: [
                    {
                        'transactionHistory.dateTime': {
                            $gte: fromDate,
                            $lt: toDate
                        }
                    },
                    {
                        [param]: { $regex: req.params.text }
                    }
                ],
            },
        })
        .sort({ 'transactionHistory.dateTime': -1 })
        .exec(function (err, result) {
            if (err) {
                res.status(500).send(err)
            } else {

                var totalEarningsCal = 0

                result.forEach(el => {
                    if (el.transactionHistory.trip != null) {
                        totalEarningsCal = totalEarningsCal + el.transactionHistory.trip.tripEarning;
                    }
                });

                responseData = result.slice(((pageNo - 1) * paginationCount), (pageNo * paginationCount));

                res.status(200).json({
                    companyWallet: responseData,
                    totalEarnings: totalEarningsCal,
                    noOfPages: result / paginationCount,
                    noOfRecords: result.length
                })

                // res.status(200).json({
                //     content: result
                // })
            }
        })
}

exports.gettripDataByTripId = function (req, res) {

    if (req.params.transactionType == 'trip') {
        Trip.findOne({ _id: mongoose.Types.ObjectId(req.params.id) })
            .exec(function (err, data) {
                if (err) {
                    console.log('####### error occured' + err);
                    res.status(400).send('error');
                } else {
                    res.json({
                        message: 'success',
                        details: "get data Successfully",
                        content: data
                    });
                }
            });
    }
    else if (req.params.transactionType == 'roadPickup') {
        RoadPickup.findOne({ _id: mongoose.Types.ObjectId(req.params.id) })
            .exec(function (err, data) {
                if (err) {
                    console.log('####### error occured' + err);
                    res.status(400).send('error');
                } else {
                    res.json({
                        message: 'success',
                        details: "get data Successfully",
                        content: data
                    });
                }
            });
    }
    else if (req.params.transactionType == 'dispatch') {
        Dispatch.findOne({ _id: mongoose.Types.ObjectId(req.params.id) })
            .exec(function (err, data) {
                if (err) {
                    console.log('####### error occured' + err);
                    res.status(400).send('error');
                } else {
                    res.json({
                        message: 'success',
                        details: "get data Successfully",
                        content: data
                    });
                }
            });
    }
    else if (req.params.transactionType == 'other') { }

}

exports.changeAndroidDriverVersion = function (req, res) {

    var newValues = {
        $set: {
            androidAppVersion : req.params.version
        }
    }

    Settings.update({}, newValues ,function (err, data) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success' })
        }
    })
}

exports.changeAndroidPassengerVersion = function (req, res) {

    var newValues = {
        $set: {
            androidUserAppVersion : req.params.version
        }
    }

    Settings.update({}, newValues ,function (err, data) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success' })
        }
    })
}

exports.changeIosPassengerVersion = function (req, res) {

    var newValues = {
        $set: {
            iosUserAppVersion : req.params.version
        }
    }

    Settings.update({}, newValues ,function (err, data) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json({ message: 'success' })
        }
    })
}