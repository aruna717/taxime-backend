'use strict';


var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var User = require('../models/user');
var UserController = require('../controllers/user');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var cryptoHandler = ('../controllers/cryptoHandler');
var Driver = require('../models/driver');
//var User = mongoose.model('User');
var sendSms = require('../services/sendSms');
var otp = require('../services/randomnum');
var imageUpload = require('./imageUpload');
var passengerWallet = require('../models/passengerwallet');
var Dispatcher = require('../models/dispatcher');
var PassengerWallet = require('../models/passengerwallet');
var Setting = require('../models/setting');

app.use(cors())
router.use(cors())



//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});


exports.user = function (req, res) {
  console.log("###### user ######");
  res.json({
    status: 'user'
  });

};

/**### send otp #### */
exports.registerOTP = function (req, res) {
  console.log("###### user register ######");
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users !== null && users.isApproved == true) {
          console.log(users);
          console.log("####################### not an null data : user already exist ##########################");
          res.status(400).send({
            message: 'failed',
            details: "You are already registered with this service. Please login!",
            status: "signup_failed"
          });
        } else {
          console.log("####################### null data ##########################");
          var user = new User();
          user.contactNumber = req.body.ContactNumber;
          user.otpPin = otp.otpGen();
          user.otpTime = new Date();

          if (users !== null) {
            User.findByIdAndRemove(users._id)
              .exec(function (err, result) {
                if (err) {
                  console.log('error!');
                } else {
                  console.log('user deleted.')
                }
              });

          }

          user.save(function (err) {
            if (err) {
              console.log('#################### error occured #######################');
              console.log(err);
              res.status(500).send(err);
            } else {
              var wallet = new passengerWallet();
              wallet.passengerId = user._id;
              wallet.save(function (err2) {
                if (err2) {
                  res.status(500).send(err2);
                } else {
                  console.log('sending sms');
                  sendSms.sendSms(req.body.ContactNumber, user.otpPin);
                  res.json({
                    message: 'success',
                    details: "Enter The code you received"
                  });
                }
              })

            }
          });
        }
      }
    });
};

/**## validate OTP ### */
exports.validateOTP = function (req, res) {
  console.log("###### user register ######");
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users !== null) {
          if ((new Date().getTime() - new Date(users.otpTime).getTime()) > 300000) {
            return res.status(209).json({
              message: 'Invalid pin'
            });
          } else if (req.body.pin !== users.otpPin) {
            return res.status(210).json({
              message: 'Pin is incorrect'
            });
          } else if (req.body.pin > 999) {
            var newValues = {
              $set: {
                otpPin: 0,
                contactNoConfirm: true
              }
            }
            User.findByIdAndUpdate(users._id, newValues, function (err, result) {
              if (err) {
                return res.status(500).json({
                  message: 'server error'
                });
              } else {

                return res.status(200).json({
                  message: 'success',
                  details: "Login successfully. Fill rest Data to complete Registration",
                  contactNo: req.body.contactNumber
                });
              }
            });
          }
        } else {
          res.status(404).json({
            message: 'User not found'
          });
        }
      }
    });
};

/* ### Signup / Register ### */
exports.register = function (req, res) {
  console.log("###### user register ######");
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users !== null && users.isApproved) {
          console.log("####################### not an null data : user already exist ##########################");
          res.status(400).send({
            message: 'failed',
            details: "contact No already registered!",
            status: "signup_failed"
          });
        } else if (users == null) {
          res.status(400).send({
            message: 'failed',
            details: "cannot find contact No",
            status: "signup_failed"
          });
        } else {
          console.log(users);

          if (users.contactNoConfirm) {

            if (req.body.email) {
              var newValues = {
                $set: {
                  name: req.body.UserName,
                  gender: req.body.Gender,
                  birthday: req.body.Birthday,
                  email: req.body.email,
                  userPlatform: req.body.UserPlatform,
                  isApproved: true
                }
              }
            } else {
              var newValues = {
                $set: {
                  name: req.body.UserName,
                  gender: req.body.Gender,
                  birthday: req.body.Birthday,
                  userPlatform: req.body.UserPlatform,
                  isApproved: true
                }
              }
            }


            User.findOneAndUpdate({
              'contactNumber': req.body.ContactNumber
            }, newValues, function (err, result) {
              if (err) {
                return res.status(500).json({
                  message: 'server error'
                });
              } else {

                imageUpload.uploadImagesCallback(req.files, users._id, User, 'users', function (response) {
                  console.log('############### Image Uploded #################');

                  // imageUpload.uploadImages(req.files, users._id, User, 'users');
                });

                setTimeout(function () {
                  User.findOne({
                      'contactNumber': req.body.ContactNumber
                    })
                    .exec(function (err, user2) {
                      if (err) {
                        console.log('####### error occured' + err);
                        res.status(400).send('error');
                      } else {
                        if (user2 !== null && user2.isApproved) {
                          console.log('********* image uploded **********');
                          console.log(user2.userProfilePic);


                          return res.status(200).json({
                            message: 'success',
                            details: "Registration Complete. Let's Servive with Snap",
                            user: user2,
                            token: jwt.sign({
                              email: req.body.ContactNumber,
                              _id: user2._id
                            }, 'RESTFULAPIs')
                          });
                        }
                      }
                    })
                }, 2000);
              }
            });
          } else {
            res.status(400).send({
              message: 'failed',
              details: "Not a confirmed contact No",
              status: "signup_failed"
            });
          }
        }
      }
    });
};

//Resend otp
exports.getOtp = function (req, res) {
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, user) {
      if (err) {
        res.status(500).json({
          message: 'internel error'
        })
      } else {
        if (user !== null) {
          console.log('## user not null ##');
          if ((new Date().getTime() - new Date(user.otpTime).getTime()) < 30000) {
            res.status(202).json({
              message: 'Too early to request'
            })
          } else {
            var pin = otp.otpGen();
            if (user.otpPin === 0 || user.otpPin > 999) {
              var newValues = {
                $set: {
                  otpPin: pin,
                  otpTime: new Date()
                }
              }
              User.findOneAndUpdate({
                'contactNumber': req.body.ContactNumber
              }, newValues, function (err, result) {
                if (err) {
                  return res.status(500).json({
                    message: 'Internal server error'
                  });
                } else {
                  sendSms.sendSms(req.body.ContactNumber, pin);
                  return res.status(200).json({
                    message: 'Pin Send Successfully'
                  });
                }
              })
            } else {
              res.status(500).json({
                message: 'internel error'
              })
            }
          }

        } else {
          res.status(204).json({
            message: 'user not found'
          });
        }
      }
    })
}

/* ### SignIn / Login ### */
exports.signIn = function (req, res) {
  console.log("###### user signIn #######");
  //res.json({status: 'sign In'});
  console.log(req.body);
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, user) {
      if (err) {
        console.log('####### error occured' + err);
        // logger.error(err)
        // res.send('error');
        res.status(400).send(err);
      } else {
        if (user !== null && user.isApproved == true) {
          console.log("####################### not an null data : user already exist ##########################");

          var pin;
          if (req.body.ContactNumber == '0775313704') {
            console.log('##block 1###')
            pin = 1111;
          } else {
            console.log('###bolck 2###')
            pin = otp.otpGen();
          }
          var newValues = {
            $set: {
              otpPin: pin,
              otpTime: new Date()
            }
          }
          var userObj = {
            userName: user.name,
            userProfilePic: user.userProfilePic
          }
          User.findOneAndUpdate({
            'contactNumber': req.body.ContactNumber
          }, newValues, function (err) {
            if (err) {
              console.log('#################### error occured #######################');
              console.log(err);
              res.send(err);
            } else {
              sendSms.sendSms(req.body.ContactNumber, pin);
              console.log(userObj)
              res.json({
                message: 'success',
                details: "Enter The code you received",
                userObj
              });
            }
          });

        } else {
          console.log("####################### null data ##########################");
          res.status(400).send({
            message: 'failed',
            details: "User not registered! Please Signup",
            status: "signup_failed"
          });
        }
      }
    });
};

/**## validate OTP ### */
exports.validateLoginOTP = function (req, res) {
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users !== null) {
          if ((new Date().getTime() - new Date(users.otpTime).getTime()) > 300000) {
            return res.status(209).json({
              message: 'Invalid pin'
            });
          } else if (req.body.pin !== users.otpPin) {
            return res.status(210).json({
              message: 'Pin is incorrect'
            });
          } else if (req.body.pin > 999) {
            var newValues = {
              $set: {
                otpPin: 0
              }
            }
            User.findOneAndUpdate({
              'contactNumber': req.body.ContactNumber
            }, newValues, function (err, result) {
              if (err) {
                return res.status(500).json({
                  message: 'server error'
                });
              } else {

                return res.status(200).json({
                  message: 'success',
                  details: "Login successfully. Fill rest Data to complete Registration",
                  ContactNumber: req.body.contactNumber,
                  user: users,
                  token: jwt.sign({
                    email: req.body.ContactNumber,
                    _id: users._id
                  }, 'RESTFULAPIs')
                });
              }
            });
          }
        } else {
          res.status(404).json({
            message: 'User not found'
          });
        }
      }
    });
};

/* ### Add fovourite location ### */
exports.addFavouriteLocation = function (req, res) {
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users == null) {
          console.log("####################### Cannot find user ##########################");
          res.status(400).send({
            message: 'failed',
            details: "Contact No cannot find",
            status: "favAdd_failed"
          });
        } else {
          console.log(req.body);
          User.findOneAndUpdate({
              'contactNumber': req.body.ContactNumber
            }, {
              $push: {
                favouriteLocations: {
                  favourName: req.body.favourName,
                  address: req.body.address,
                  latitude: req.body.latitude,
                  longitude: req.body.longitude
                }
              }
            })
            .exec(function (err, data) {
              if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
              } else {
                console.log(data);

                res.json({
                  message: 'success',
                  details: "Successfully added Location",
                  content: {
                    address: req.body.address,
                    latitude: req.body.latitude,
                    longitude: req.body.longitude
                  }
                });
              }

            });
        }
      }
    });
};

/* ### Add fovourite location ### */
exports.removeFavouriteLocation = function (req, res) {
  User.findOne({
      'contactNumber': req.body.ContactNumber
    })
    .exec(function (err, users) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (users == null) {
          console.log("####################### Cannot find user ##########################");
          res.status(400).send({
            message: 'failed',
            details: "Contact No cannot find",
            status: "favAdd_failed"
          });
        } else {
          console.log(req.body);

          User.update({
              'contactNumber': req.body.ContactNumber
            }, {
              $pull: {
                "favouriteLocations": {
                  "_id": req.body._id
                }
              }
            })
            .exec(function (err, data) {
              if (err) {
                console.log('####### error occured' + err);
                res.status(400).send('error');
              } else {
                console.log(data);

                res.json({
                  message: 'success',
                  details: "Successfully remove Location"
                });
              }

            });
        }
      }
    });
};

/**get favourite locations by user id */
exports.getFavouriteLocation = function (req, res) {

  console.log("###### get Favourite locations ######");
  User.find({
      'contactNumber': req.params.ContactNumber
    })
    .exec(function (err, data) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(400).send('error');
      } else {
        if (data == null || data == []) {
          res.status(400).json({
            message: 'failed',
            details: "No data found",
            status: "failed"
          });
        } else {
          if (data[0]) {
            res.json({
              message: 'success',
              details: "get data Successfully",
              content: data[0].favouriteLocations
            });
          } else {
            res.json({
              message: 'success',
              details: "Data is empty"
            });
          }
        }
      }
    });

};

// authorise driver //
exports.userLoginRequired = function (req, res, next) {
  console.log("###### login required ######");
  console.log(req.headers.authorization);
  console.log(req.headers['content-type'])
  console.log(req.user)
  //res.json({status: 'login required'});
  if (req.user) {
    next();
  } else {
    return res.status(401).json({
      message: 'Unauthorized driver!'
    });
  }
};

exports.checkInfo = function (req, res) {
  console.log(req.headers.authorization)
  User.aggregate([{
        $match: {
          '_id': mongoose.Types.ObjectId(req.body.userId)
        }
      },
      {
        $lookup: {
          from: "dispatchers",
          localField: "_id",
          foreignField: "dispatcherId",
          as: "dispatcher"
        }
      },
      {
        $lookup: {
          from: "passengerwallets",
          localField: "_id",
          foreignField: "passengerId",
          as: "wallet"
        }
      },
      {
        $project: {
          'otpPin': 0,
          'saltSecret': 0,
          'otpTime': 0
        }
      }
    ])
    .exec(function (err, user) {
      if (err) {
        res.status(500).send(err);
      } else {
        if (user != null) {
          console.log(user)
          res.status(206).json({
            message: 'User details',
            content1: user,
            // content2: null
          });
        } else {
          res.status(202).json({
            message: 'User is not approved'
          });
        }
      }
    });

}

exports.addDispatcherReferalCode = function (req, res) {

  Dispatcher.findOne({
      'dispatcherCode': req.body.referralId
    })
    .exec(function (err, data) {
      if (err) {
        console.log('####### error occured' + err);
        res.status(500).send('error');
      } else {
        if (data == null) {

          Driver.findOne({
              'driverCode': req.body.referralId
            })
            .exec(function (err, data1) {
              if (err) {
                console.log('####### error occured' + err);
                res.status(500).send('error');
              } else {
                if (data1 == null) {

                  res.status(400).send({
                    message: 'failed',
                    details: "Invalid Code!",
                    status: "code_failed"
                  });
                } else {

                  var refType = 'driver'

                  var d = new Date();
                  var year = d.getFullYear();
                  var month = d.getMonth();
                  var day = d.getDate();
                  var c = new Date(year + 1, month, day)

                  var newValues = {
                    $push: {
                      referral: {
                        referralId: req.body.referralId,
                        referredDriverId: data1._id,
                        referredId: data1._id,
                        referredType: refType,
                        earning: 1,
                        recordedDate: new Date(),
                        expireDate: c
                      }
                    }
                  }

                  PassengerWallet.findOneAndUpdate({
                    'passengerId': req.body.passengerId
                  }, newValues, function (err, result) {
                    if (err) {
                      return res.status(500).json({
                        message: err
                      });
                    } else {
                      res.json({
                        message: 'success',
                        details: "Successfully added Refferal Code"
                      });
                    }
                  });

                }
              }
            })
        } else {

          if (data.type == 'Driver') {
            var refType = 'driverDispatcher'
          } else if (data.type == 'User') {
            var refType = 'userDispatcher'
          }

          var d = new Date();
          var year = d.getFullYear();
          var month = d.getMonth();
          var day = d.getDate();
          var c = new Date(year + 1, month, day)

          var newValues = {
            $push: {
              referral: {
                referralId: req.body.referralId,
                referredDriverId: data.dispatcherId,
                referredId: data.dispatcherId,
                referredType: refType,
                earning: 1,
                recordedDate: new Date(),
                expireDate: c
              }
            }
          }

          PassengerWallet.findOneAndUpdate({
            'passengerId': req.body.passengerId
          }, newValues, function (err, result) {
            if (err) {
              return res.status(500).json({
                message: err
              });
            } else {
              res.json({
                message: 'success',
                details: "Successfully added Refferal Code"
              });
            }
          });
        }
      }
    });


}


exports.getTrips = function (req, res) {

  var fromDate = new Date(req.params.from);
  var toDate = new Date(req.params.to);

  PassengerWallet.aggregate({
      $match: {
        dispatcherId: mongoose.Types.ObjectId(req.params.id)
      }
    }, {
      "$unwind": "$transactionHistory"
    }, {
      $match: {
        $and: [{
          'transactionHistory.dateTime': {
            $gte: fromDate,
            $lt: toDate
          }
        }],
      },
    })
    .exec(function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {

        res.status(200).json({
          passengerWallet: result
        })
      }
    })


}

exports.getLatestAndroidUserVersion = function (req, res) {

  Setting.find({}, function (err, data) {
    if (err) {
      console.log('#################### error occured #######################');
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).json({
        message: 'success',
        // darta : data
        androidAppUserVersion: data[0].androidUserAppVersion
      });
    }
  });

}

exports.getLatestIosUserVersion = function (req, res) {

  Setting.find({}, function (err, data) {
    if (err) {
      console.log('#################### error occured #######################');
      console.log(err);
      res.status(500).send(err);
    } else {
      res.status(200).json({
        message: 'success',
        // darta : data
        androidAppUserVersion: data[0].iosUserAppVersion
      });
    }
  });

}

exports.editPassenger = function (req, res) {

  User.findById(req.body.userId)
    .exec(function (err, user) {
      if (err) {
        res.status(500).json({
          message: 'Server Error'
        });
      } else {
        if (user.address.length > 0) {

          var newValues = {
            $set: {
              name: req.body.userName,
              gender: req.body.gender,
              birthday: req.body.birthday,
              email: req.body.email,
              'address.0.address': req.body.address,
              'address.0.state': req.body.state,
              'address.0.city': req.body.city,
              'address.0.zipcode': req.body.zipcode,
              'address.0.country': req.body.country
            }
          }

          User.findByIdAndUpdate(req.body.userId, newValues, function (err, results) {
            if (err) {
              console.log(err)
              res.status(500).json({
                message: 'Server Error!'
              });
            } else {
              if (results != null) {
                console.log(results);
                res.status(200).json({
                  message: 'success!'
                });
              } else {
                console.log(results)
                res.status(500).json({
                  message: 'failed!'
                });
              }

            }
          })

        } else {
          var newValues1 = {
            $set: {
              name: req.body.userName,
              gender: req.body.gender,
              birthday: req.body.birthday,
              email: req.body.email
            },
            $push: {
              address: {
                address: req.body.address,
                state: req.body.state,
                city: req.body.city,
                zipcode: req.body.zipcode,
                country: req.body.country
              }
            }
          }

          User.findByIdAndUpdate(req.body.userId, newValues1, function (err, results) {
            if (err) {
              console.log(err)
              res.status(500).json({
                message: 'Server Error!'
              });
            } else {
              if (results != null) {
                console.log(results);
                res.status(200).json({
                  message: 'success!'
                });
              } else {
                console.log(results)
                res.status(500).json({
                  message: 'failed!'
                });
              }

            }
          })

        }
      }
    })

}


/* ### SocialMedia Signup / Social media login ### */
// exports.SocialMediaLoginRegister = function (req, res) {
//   console.log('writing new user');
//   console.log(req.body);

//   var user = new User();
//   var socialLogin = mongoose.Types.ObjectId();

//   user.email = req.body.Email;
//   user.userType = req.body.UserType;
//   user.socialLoginId = req.body.SocialLoginId;
//   user.userProfilePic = req.body.UserProfilePic;

//   User.findOne({ 'email': req.body.Email })
//     .exec(function (err, users) {
//       if (err) {
//         console.log('error occured');
//         res.send('error');
//       } else {
//         if (users !== null) {
//           //social media registed email registered already
//           console.log("####################### not an null data : user already exist : login in action performing ##########################");
//           // console.log(users);
//           if (users.socialLoginId == req.body.SocialLoginId) {
//             console.log("=========> login successful : pass key matched !");
//             users.password = undefined;
//             res.json({
//               message: 'success',
//               details: "login successful",
//               status: "login_success",
//               content: users
//             });
//           } else {
//             console.log("=========> wrong access key");
//             res.json({ message: 'failed', details: "login failed on wrong access key", status: "login_failed" });
//           }

//         } else {
//           console.log("####################### null data ##########################");
//           // console.log(users);
//           user.password = socialLogin;
//           user.fisrtName = req.body.firstName;
//           user.lastName = req.body.lastName
//           user.contactNumber = 'none';
//           user.isEnableUser = true;
//           user.birthday[0] = 'none';
//           //user.address = 'none';
//           user.userPlatform = req.body.UserPlatform;

//           user.save(function (err) {
//             if (err) {
//               console.log('#################### error occured #######################');
//               console.log(err);
//               res.send(err);
//             } else {
//               console.log('==============> Social User added successfully !');
//               sendEmail(req.body.Email, req.body.Name);
//               User.findOne({ 'email': req.body.Email })
//                 .exec(function (err, user) {
//                   if (err) {
//                     console.log('error occured');
//                     res.send('error');
//                   } else {
//                     res.json({ message: 'success', details: "Registed successfully", content: user });
//                   }
//                 });
//             }
//           });
//         }
//       }
//     });
// };




/* ### Update user Profile  ### */
// exports.updateProfile = function (req, res) {
//   console.log('###### updating user profile ######');
//   User.findById(req.body.UserId)
//     .exec(function (err, user) {
//       if (err) {
//         console.log('error occured');
//         console.log(err)
//         res.json({ message: 'failed', details: "User does not exists", status: "user_not_exited" });
//       }
//       else {
//         if (user !== null) {
//           var newValues = {
//             $set: {
//               birthDay: req.body.BirthDay,
//               firstName: req.body.FirstName,
//               lastName: req.body.LastName,
//               contactNumber: req.body.ContactNumber,
//               address: req.body.Address
//             }
//           }
//           User.findByIdAndUpdate(req.body.UserId, newValues, function (err, result) {
//             if (err) {
//               console.log(err)
//               throw err;
//             } else {
//               User.findById(req.body.UserId)
//                 .exec(function (err, user) {
//                   if (err) {
//                     console.log('error occured');
//                     console.log(err)
//                   } else {
//                     res.json({ message: 'success', details: "user profile updated successfully", content: user });
//                   }
//                 });
//             }
//           });
//         } else {
//           res.json({ message: 'failed', details: "User does not exists", status: "user_not_exited" });
//         }
//       }
//     });
// };


// /* ### Change user password ### */
// exports.updatePassword = function (req, res) {
//   console.log('###### updating password ######');
//   User.findById(req.body.UserId)
//     .exec(function (err, user) {
//       if (err) {
//         console.log('error occured');
//         console.log(err)
//         res.json({ message: 'failed', details: "User does not exists", status: "user_not_exited" });
//       }
//       else {
//         if (user !== null) {
//           if (bcrypt.compareSync(req.body.OldPassword, user.password)) {
//             var newValues = {
//               $set: {
//                 password: bcrypt.hashSync(req.body.NewPassword, 10)
//               }
//             }
//             User.findByIdAndUpdate(req.body.UserId, newValues, function (err, result) {
//               if (err) {
//                 console.log(err)
//                 throw err;
//               } else {
//                 User.findById(req.body.UserId)
//                   .exec(function (err, user) {
//                     if (err) {
//                       console.log('error occured');
//                       console.log(err)
//                     } else {
//                       res.json({ message: 'success', details: "user profile updated successfully", content: user });
//                     }
//                   });
//               }
//             });
//           } else {
//             res.json({ message: 'failed', details: "Current password doesn't matched!", status: "authentification_failed" });
//           }

//         } else {
//           res.json({ message: 'failed', details: "User does not exists", status: "user_not_exited" });
//         }
//       }
//     });
// };

// exports.checkEmail = function (req, res) {
//   console.log('###### checkingEmail ######');
//   User.findOne({ 'email': req.body.Email })
//     .exec(function (err, user) {
//       if (err) {
//         console.log('####### error occured' + err);
//         // logger.error(err)
//         res.send('error');
//       } else {
//         if (user !== null) {
//           console.log("####################### not an null data : user already exist ##########################");
//           res.json({ message: 'success', details: "Email already registed" });
//         } else {
//           res.json({ message: 'failed', details: "Email not registed" });
//         }
//       }
//     });
// };