'use strict';


var express = require('express');
var fileUpload = require('express-fileupload');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var config = require('../config');
var Driver = require('../models/driver');
var User = require('../models/user');
var Vehicle = require('../models/vehicle');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var cryptoHandler = ('../controllers/cryptoHandler');
var rand = require('../services/randomnum');

app.use(cors())
router.use(cors())

app.use(fileUpload());



//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

exports.uploadImages = function (req, objId, modelName, folderName) {
  if (!req) {
    console.log('no req');
  } else {
    let id = new mongoose.Types.ObjectId(objId);

    modelName.findById(id)
      .exec(function (err, result) {
        if (err) {
          console.log('errr')
        } else {
          if (result !== null) {
            var imageFirstName = rand.imagename();
            for (let key in req) {
              req[key].mv('./public/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1], function (err) {
                if (err) {
                  console.log('err1');
                } else {
                  var imagePath = { $set: {} }
                  imagePath.$set[key] = '/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1]
                  modelName.findByIdAndUpdate(id, imagePath, function (err, result) {
                    if (err) {
                      console.log('err2');
                    } else {
                      console.log('successs!');
                    }
                  })
                }
              });
            }
          }
          else {
            console.log('err');
          }
        }
      });
  }
};

exports.uploadImagesCallback = function (req, objId, modelName, folderName, callback) {
  if (!req) {
    console.log('no req');
  } else {
    let id = new mongoose.Types.ObjectId(objId);

    modelName.findById(id)
      .exec(function (err, result) {
        if (err) {
          console.log('errr')
        } else {
          if (result !== null) {
            var imageFirstName = rand.imagename();
            for (let key in req) {
              req[key].mv('./public/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1], function (err) {
                if (err) {
                  console.log('err1');
                } else {
                  var imagePath = { $set: {} }
                  imagePath.$set[key] = '/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1]
                  modelName.findByIdAndUpdate(id, imagePath, function (err, result) {
                    if (err) {
                      console.log('err2');
                    } else {
                      console.log('successs!');
                    }
                  })
                }
              });
            }
            callback(1);
          }
          else {
            console.log('err');
          }
        }
      });
  }
};


exports.uploadImagesInSubDocuments = function (req, ParentId, ChildId, modelName, folderName) {
  console.log('inside image upload')
  if (!req) {
    console.log('no req');
  } else {
    let parentId = new mongoose.Types.ObjectId(ParentId);
    let childId = new mongoose.Types.ObjectId(ChildId);

    modelName.findById(parentId)
      .exec(function (err, result) {
        if (err) {
          console.log('err1')
        } else {
          if (result !== null) {
            var imageFirstName = rand.imagename();
            for (let key in req) {
              req[key].mv('./public/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1], function (err) {
                if (err) {
                  console.log('err2');
                } else {
                  // var imagePath = { $set: {} }
                  var imagePath = '/images/' + folderName + '/' + imageFirstName + '_' + key + '.' + req[key].mimetype.split('/')[1]

                  // if (key === 'mapIcon') {

                    var setModifier = { $set: {} };
                    setModifier.$set['subCategory.$.' + key ] = imagePath;

                  modelName.update({
                    _id: parentId,
                    "subCategory._id": childId
                  }, setModifier
                  )
                  .exec(function(err, result) {
                    if(err) {
                      console.log("err3");
                    } else {
                      console.log(result)
                    }
                  })
                  
                  

                  // modelName.findByIdAndUpdate(parentId, imagePath, function (err, result) {
                  //   if (err) {
                  //     console.log('err');
                  //   } else {
                  //     console.log('successs!');
                  //   }
                  // })
                }
              });
            }
          }
          else {
            console.log('err');
          }
        }
      });
  }
};

