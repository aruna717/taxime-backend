'use strict';

var nodemailer = require('nodemailer');
var config = require('../config');
const sgMail = require('@sendgrid/mail');


exports.driverRegEmail = function (driverEmail, salt) {

  sgMail.setApiKey(config.SENDGRID_API_KEY);

  var link = config.SERVER_URL + '/driver/confirmemail/' + salt;

  const msg = {
    to: `${driverEmail}`,
    from: 'info@snaplk.com',
    subject: 'Snap',
    text: 'Thank you for registering with Snap',
    html: `<p>Please click <a href=${link}>here</a> to confirm your Email.</p>`,
  };


  sgMail.send(msg, function (err, info) {
    if (err) {
      return error;
    } else {
      console.log('Email Sent');
    }
  });
}