'use strict';

var nodemailer = require('nodemailer');

exports.adminRegEmail = function (adminEmail, salt) {

  var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    auth: {
      user: 'projectaxeman@gmail.com',
      pass: 'axeman2018'
    },
    tls: {
      rejectUnauthorized: false //unathoutized access allow
    }
  });

  var link = config.SERVER_URL +'/admin/confirmemail/' + salt;

  var mailOptions = {
    from: 'projectaxeman@gmail.com',
    to: `${adminEmail}`,
    subject: 'Snap',
    text: 'Thank you for registering with Snap',
    html: `<p>Please click <a href=${link}>here</a> to confirm your Email.</p>`
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return error;
    } else {
      console.log('Email sent');
    }
  });
}