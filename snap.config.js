module.exports = {
    apps : [{
      name        : "snap_api",
      script      : "app.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/home/apexzonedemo/taxiapp/snapbackend",
     },
     {
      name        : "snap_admin_socket",
      script      : "socket.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/home/apexzonedemo/taxiapp/snapbackend",
     },
     {
      name        : "snap_driver_socket",
      script      : "driversocket.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/home/apexzonedemo/taxiapp/snapbackend",
     },
     {
      name        : "snap_passenger_socket",
      script      : "passengersocket.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/home/apexzonedemo/taxiapp/snapbackend",
     }
]
  }

//   //test
// module.exports = {
//   apps: [{
//       name: "snap_api_test",
//       script: "app.js",
//       watch: true,
//       merge_logs: true,
//       cwd: "/home/chathura070/snapTest2/snapbackend",
//     },
//     {
//       name: "snap_admin_socket_test",
//       script: "socket.js",
//       watch: true,
//       merge_logs: true,
//       cwd: "/home/chathura070/snapTest2/snapbackend",
//     },
//     {
//       name: "snap_driver_socket_test",
//       script: "driversocket.js",
//       watch: true,
//       merge_logs: true,
//       cwd: "/home/chathura070/snapTest2/snapbackend",
//     },
//     {
//       name: "snap_passenger_socket_test",
//       script: "passengersocket.js",
//       watch: true,
//       merge_logs: true,
//       cwd: "/home/chathura070/snapTest2/snapbackend",
//     }
//   ]
// }