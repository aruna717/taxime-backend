const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
// var notificationStats = require('./services/notificationstats');
const Driver = require('./models/driver');
const VehicleTrack = require('./models/vehicletracking');
const VehicleCategory = require('./models/vehiclecategory');
const app = express();
var mongoose = require('mongoose');
var config = require('./config');
var notification = require('./services/adminNotifications');

app.get('/', (req, res) => {
    res.send("Socket server");
});

var cors = require('cors');
app.use(cors());


const server = http.Server(app);
const portSelected = config.DRIVER_SOCKET_PORT;

server.listen(portSelected, function () {
    console.log('SNAP API driver Socket listening on port 8099');
});

var dbe = config.DB_URL;

mongoose.Promise = require('bluebird');
mongoose.connect(dbe, {
    useMongoClient: true
});

const io = socketIo(server);

io.on('connection', (socket) => {
    console.log('connection: ' + socket.id);
    socket.on('driverConnected', async (res) => {
        console.log('driverConnected')
        console.log(res)
        // var driVals = {
        //     $set: {
        //         vehicleId : res.vehicleId,
        //         driverId : res.driverId,
        //         socketId : socket.id,
        //         'currentLocation.address' : res.address,
        //         'currentLocation.latitude' : res.latitude,
        //         'currentLocation.longitude' : res.longitude,
        //         vehicleCategory : res.vehicleCategory,
        //         vehicleSubCategory : res.vehicleSubCategory,
        //         operationRadius : res.operationRadius,
        //         'driverInfo.driverName' : res.driverName,
        //         'driverInfo.driverContactNumber' : res.driverContactNumber,
        //         'vehicleInfo.vehicleRegistrationNo' : res.vehicleRegistrationNo,
        //         'vehicleInfo.vehicleLicenceNo' : res.vehicleLicenceNo,
        //         currentStatus : res.currentStatus
        //     }
        // }

        // VehicleTrack.findOneAndUpdate({
        //     driverId: res.driverId
        // }, driVals, function (error, driverTrack) {
        //     if (driverTrack != null) {
        //         io.to(socket.id).emit('driverConnectResult', driverTrack);
        //     } else {

        VehicleTrack.findOneAndRemove({
                'driverId': res.driverId
            })
            .exec(function (err, result) {
                var vehicletrack = new VehicleTrack();
                vehicletrack.vehicleId = res.vehicleId;
                vehicletrack.driverId = res.driverId;
                vehicletrack.socketId = socket.id
                vehicletrack.currentLocation.address = res.address;
                vehicletrack.currentLocation.latitude = res.latitude;
                vehicletrack.currentLocation.longitude = res.longitude;
                vehicletrack.vehicleCategory = res.vehicleCategory;
                vehicletrack.vehicleSubCategory = res.vehicleSubCategory;
                vehicletrack.operationRadius = res.operationRadius;
                vehicletrack.driverInfo.driverName = res.driverName;
                vehicletrack.driverInfo.driverContactNumber = res.driverContactNumber;
                vehicletrack.vehicleInfo.vehicleRegistrationNo = res.vehicleRegistrationNo;
                vehicletrack.vehicleInfo.vehicleLicenceNo = res.vehicleLicenceNo;
                vehicletrack.currentStatus = res.currentStatus;
                // vehicletrack.bidValue = res.bidValue;
                vehicletrack.driverPic = res.driverPic;
                vehicletrack.subCategoryIcon = res.subCategoryIcon;
                vehicletrack.subCategoryIconSelected = res.subCategoryIconSelected;
                vehicletrack.mapIcon = res.mapIcon;
                vehicletrack.mapIconOffline = res.mapIconOffline;
                vehicletrack.mapIconOntrip = res.mapIconOntrip;
                vehicletrack.subCategoryIconSVG = res.subCategoryIconSVG;
                vehicletrack.subCategoryIconSelectedSVG = res.subCategoryIconSelectedSVG;
                vehicletrack.mapIconSVG = res.mapIconSVG;
                vehicletrack.mapIconOfflineSVG = res.mapIconOfflineSVG;
                vehicletrack.mapIconOntripSVG = res.mapIconOntripSVG;

                // if(res.vehicleColor && res.vehicleBrand && res.totalWalletPoints) {
                //     vehicletrack.vehicleInfo.vehicleColor = res.vehicleColor;
                //     vehicletrack.vehicleInfo.vehicleBrand = res.vehicleBrand;
                //     vehicletrack.totalWalletPoints = res.totalWalletPoints;
                // }

                vehicletrack.save(function (err, results) {
                    if (err) {
                        console.log('inVtrackErr: ' + err)
                        //io.to(socket.id).emit('driverConnectResult', 'faild!');
                    } else {
                        console.log('VtrackSave: ' + results)
                        io.to(results.socketId).emit('driverConnectResult', results);
                    }
                })
            })

        /// }
        //})


    });

    socket.on('updateCurrentStatus', (res) => {
        var newVals = {
            $set: {
                'currentStatus': res.currentStatus
            }
        }

        VehicleTrack.findOneAndUpdate({
            socketId: res.to
        }, newVals, function (err, result) {
            if (err) {
                console.log('stateUpdateErr: ' + err);
                io.to(res.to).emit('currentStateUpdate', 'failed');
            } else {
                // console.log('stateUpdateRes: ' + result)
                io.to(res.to).emit('currentStateUpdate', result);
            }
        })
    })

    socket.on('submitLocation', (res) => {
        if (res.driverId && res.vehicleId) {
            if (res.address != null) {
                var newValues = {
                    $set: {
                        'currentLocation.longitude': res.longitude,
                        'currentLocation.latitude': res.latitude,
                        'currentLocation.address': res.address,
                        currentStatus: res.currentStatus,
                        //bidValue : res.bidValue,
                        driverPic : res.driverPic,
                        subCategoryIcon : res.subCategoryIcon,
                        subCategoryIconSelected : res.subCategoryIconSelected,
                        mapIcon : res.mapIcon,
                        mapIconOffline : res.mapIconOffline,
                        mapIconOntrip : res.mapIconOntrip,
                        subCategoryIconSVG : res.subCategoryIconSVG,
                        subCategoryIconSelectedSVG : res.subCategoryIconSelectedSVG,
                        mapIconSVG : res.mapIconSVG,
                        mapIconOfflineSVG : res.mapIconOfflineSVG,
                        mapIconOntripSVG : res.mapIconOntripSVG
                    }
                }
                VehicleTrack.findOneAndUpdate({
                    $or: [{
                        'socketId': res.to
                    }, {
                        'driverId': res.driverId
                    }]
                }, newValues, function (err, result) {
                    if (err) {
                        console.log('locationUErr: ' + err)
                        //io.to(res.to).emit('gotLocation', 'fail');
                    } else {
                        if (result == null && res.to != null) {
                            var vehicletrack = new VehicleTrack();
                            vehicletrack.vehicleId = res.vehicleId;
                            vehicletrack.driverId = res.driverId;
                            vehicletrack.socketId = res.to;
                            vehicletrack.currentLocation.address = res.address;
                            vehicletrack.currentLocation.latitude = res.latitude;
                            vehicletrack.currentLocation.longitude = res.longitude;
                            vehicletrack.vehicleCategory = res.vehicleCategory;
                            vehicletrack.vehicleSubCategory = res.vehicleSubCategory;
                            vehicletrack.operationRadius = res.operationRadius;
                            vehicletrack.driverInfo.driverName = res.driverName;
                            vehicletrack.driverInfo.driverContactNumber = res.driverContactNumber;
                            vehicletrack.vehicleInfo.vehicleRegistrationNo = res.vehicleRegistrationNo;
                            vehicletrack.vehicleInfo.vehicleLicenceNo = res.vehicleLicenceNo;
                            vehicletrack.currentStatus = res.currentStatus;
                           // vehicletrack.bidValue = res.bidValue;
                            vehicletrack.driverPic = res.driverPic;
                            vehicletrack.subCategoryIcon = res.subCategoryIcon;
                            vehicletrack.subCategoryIconSelected = res.subCategoryIconSelected;
                            vehicletrack.mapIcon = res.mapIcon;
                            vehicletrack.mapIconOffline = res.mapIconOffline;
                            vehicletrack.mapIconOntrip = res.mapIconOntrip;
                            vehicletrack.subCategoryIconSVG = res.subCategoryIconSVG;
                            vehicletrack.subCategoryIconSelectedSVG = res.subCategoryIconSelectedSVG;
                            vehicletrack.mapIconSVG = res.mapIconSVG;
                            vehicletrack.mapIconOfflineSVG = res.mapIconOfflineSVG;
                            vehicletrack.mapIconOntripSVG = res.mapIconOntripSVG;

                            vehicletrack.save(function (err, results) {
                                if (err) {
                                    console.log('inVtrackErr: ' + err)
                                    //io.to(socket.id).emit('driverConnectResult', 'faild!');
                                } else {
                                    // console.log('VtrackSaveInlocation: ' + results)
                                    io.to(results.socketId).emit('driverConnectResult', results);
                                }
                            })
                        } else {
                            // console.log('location updated');
                            // console.log(result)
                        }
                    }
                })
            }
        } else {
            if (res.address != null) {
                var newValues = {
                    $set: {
                        'currentLocation.longitude': res.longitude,
                        'currentLocation.latitude': res.latitude,
                        'currentLocation.address': res.address
                    }
                }
                VehicleTrack.findOneAndUpdate({
                    $or: [{
                        'socketId': res.to
                    }, {
                        'driverId': res.driverId
                    }]
                }, newValues, function (err, result) {
                    if (err) {
                        console.log('locationUErr: ' + err)
                        //io.to(res.to).emit('gotLocation', 'fail');
                    } else {
                        //io.to(res.to).emit('gotLocation', result);
                    }
                })
            }
        }


    })

    socket.on('DispatchTrip', (data) => {
        // console.log(data.trip)
        io.to(data.socketId).emit('dispatch', data.trip);
    });

    socket.on('PassengerTrip', (data) => {
        console.log('live trip')
        // console.log(data.trip)
        io.to(data.socketId).emit('passengertrip', data.trip);
    });

    socket.on('RemoveTrip', (data) => {
        console.log('remove from socket')
        console.log(data.trip)
        io.to(data.socketId).emit('removeDispatch', data.trip);
    });

    socket.on('PassengerCancelTrip', (data) => {
        console.log('passenger cancel live trip')
        console.log(data)
        io.to(data.socketId).emit('passengercanceltrip', data);
    });

    socket.on('disconnect', () => {
        console.log('disconnecting');
        VehicleTrack.findOneAndRemove({
                'socketId': socket.id
            })
            .exec(function (err, result) {
                if (err) {
                    console.log('err')
                } else {
                    // console.log('vehicle track deleted.')
                }
            })
    })

    socket.on('toDisconnect', (data) => {
        console.log('disconnecting by driver');
        VehicleTrack.findOneAndRemove({
                $or: [{
                    'socketId': data.socketId
                }, {
                    'driverId': data.driverId
                }]
            })
            .exec(function (err, result) {
                if (err) {
                    console.log('err')
                } else {
                    console.log('vehicle track deleted.')
                }
            })
    })

    socket.on('recon', (data) => {
        console.log('reconnected');
        var newVals = {
            $set: {
                'socketId': socket.id
            }
        }

        VehicleTrack.findOneAndUpdate({
            driverId: data.driverId
        }, newVals, function (err, results) {
            if (err) {
                console.log('err in reconnecting')
                //io.to(socket.id).emit('reConnectResult', 'faild!');
            } else {
                if (results != null) {
                    console.log('reconnect success!')
                    // console.log(results);
                    io.to(socket.id).emit('reConnectResult', results);
                } else {
                    if (data.vehicleId && data.driverName) {
                        // console.log('reconnect success!')
                        //console.log(results);
                        //console.log(socket.id)
                        //console.log(data)
                        if (data.address != null) {
                            var vehicletrack = new VehicleTrack();
                            vehicletrack.vehicleId = data.vehicleId;
                            vehicletrack.driverId = data.driverId;
                            vehicletrack.socketId = socket.id
                            vehicletrack.currentLocation.address = data.address;
                            vehicletrack.currentLocation.latitude = data.latitude;
                            vehicletrack.currentLocation.longitude = data.longitude;
                            vehicletrack.vehicleCategory = data.vehicleCategory;
                            vehicletrack.vehicleSubCategory = data.vehicleSubCategory;
                            vehicletrack.operationRadius = data.operationRadius;
                            vehicletrack.driverInfo.driverName = data.driverName;
                            vehicletrack.driverInfo.driverContactNumber = data.driverContactNumber;
                            vehicletrack.vehicleInfo.vehicleRegistrationNo = data.vehicleRegistrationNo;
                            vehicletrack.vehicleInfo.vehicleLicenceNo = data.vehicleLicenceNo;
                            vehicletrack.currentStatus = data.currentStatus;
                            // vehicletrack.bidValue = data.bidValue;
                            vehicletrack.driverPic = data.driverPic;
                            vehicletrack.subCategoryIcon = data.subCategoryIcon;
                            vehicletrack.subCategoryIconSelected = data.subCategoryIconSelected;
                            vehicletrack.mapIcon = data.mapIcon;
                            vehicletrack.mapIconOffline = data.mapIconOffline;
                            vehicletrack.mapIconOntrip = data.mapIconOntrip;
                            vehicletrack.subCategoryIconSVG = data.subCategoryIconSVG;
                            vehicletrack.subCategoryIconSelectedSVG = data.subCategoryIconSelectedSVG;
                            vehicletrack.mapIconSVG = data.mapIconSVG;
                            vehicletrack.mapIconOfflineSVG = data.mapIconOfflineSVG;
                            vehicletrack.mapIconOntripSVG = data.mapIconOntripSVG;

                            vehicletrack.save(function (error1, results1) {
                                if (err) {
                                    console.log('inVtrackErr: ' + error1)
                                    io.to(socket.id).emit('driverConnectResult', 'faild!');
                                } else {
                                    if (results1 != null) {
                                        console.log('vtrack:')
                                        //console.log(results1)
                                        io.to(results1.socketId).emit('driverConnectResult', results1)
                                    }
                                }
                            })
                        }
                        // io.to(socket.id).emit('reConnectResult', results);

                    } else {
                        //console.log('inside else');
                        io.to(socket.id).emit('reConnectResult', results);
                    }

                }
            }
        })
    })
});