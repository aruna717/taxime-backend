'use strict';


var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors')
var Driver = require('../models/driver');
var socket = require('socket.io-client')('http://localhost:8099');


app.use(cors())
router.use(cors())


//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// exports.driverRegToAdmin = function (req) {
//     console.log('notify')
//     socket.emit('newDriver', req);
// };

// exports.driverStatus = function (req) {
//     console.log('driver state')
//     socket.emit('driverConnect', req);
// };

exports.sendLiveTripToDriver = function (req) {
    console.log('sending live trip to driver socket!');
    socket.emit('PassengerTrip', req);
}

exports.sendDispatchToDriver = function (req) {
    console.log('sending dispatch to driver socket!');
    socket.emit('DispatchTrip', req);
}

exports.removeDispatch = function (req) {
    console.log('sending dispatch remove to driver socket');
    socket.emit('RemoveTrip', req);
}

exports.removeLiveTrip = function (req) {
    console.log('sending livetrip remove to driver socket');
    socket.emit('RemoveTrip', req);
}

exports.sendLiveTripCancelToDriver =  function(req){
    console.log('sending livetrip cancel to driver socket');
    socket.emit('PassengerCancelTrip', req);
}