var rn = require('random-number');

exports.imagename = function() {
    console.log('### generating randno ###')
    var options = {
        min:  100000000000
        , max: 100000000000000000
        , integer: true
    }
    var abc = rn(options);
    return abc;
}

exports.otpGen = function() {
    var options = {
        min: 1000,
        max: 9999,
        integer: true
    }
    var otp = rn(options);
    return otp;
}


exports.rndNo = function() {
    var options = {
        min: 5,
        max: 10,
        integer: true
    }
    var otp = rn(options);
    return otp;
}