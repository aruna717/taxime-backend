'use strict'

var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var User = require('../models/user');
var UserController = require('../controllers/user');
var AdminController = require('../controllers/admin');
app.use(cors())
router.use(cors())



//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// router.get('/', UserController.loginRequired, UserController.user);
router.post('/registerOTP', UserController.registerOTP);
router.post('/validateOTP', UserController.validateOTP);
router.post('/signup', UserController.register);

router.post('/resendOTP', UserController.getOtp);

router.post('/login', UserController.signIn);
router.post('/validateLoginOTP', UserController.validateLoginOTP);


router.post('/addFavouriteLocation', UserController.userLoginRequired, UserController.addFavouriteLocation);
router.post('/removeFavouriteLocation', UserController.userLoginRequired, UserController.removeFavouriteLocation);
router.get('/getFavouriteLocation/:ContactNumber', UserController.userLoginRequired, UserController.getFavouriteLocation);

router.post('/checkInfo', /*UserController.userLoginRequired,*/ UserController.checkInfo);

router.post('/addDispatcherReferalCode', /*UserController.userLoginRequired,*/ UserController.addDispatcherReferalCode);

router.get('/getTrips/:id/:from/:to', UserController.userLoginRequired, UserController.getTrips);

router.get('/getLatestUserAndroidVersion', UserController.getLatestAndroidUserVersion);
router.get('/getLatestIosUserVersion', UserController.getLatestIosUserVersion);
router.post('/updatepassenger', AdminController.adminloginRequired, UserController.editPassenger);

// router.put('/update', UserController.loginRequired, UserController.updateProfile);
// router.put('/updatePassword', UserController.loginRequired, UserController.updatePassword);
// router.post('/checkEmail', UserController.checkEmail);
// router.post('socialLogin', UserController.SocialMediaLoginRegister);
//router.post('/loginreq', UserController.loginRequired); //Auth-Token-Test

// router.post('/forgotPassword');
// router.post('/changePassword');
// router.post('/imageUpload');

module.exports = router;
