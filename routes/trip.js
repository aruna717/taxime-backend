'use strict'

var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors')
var RoadPickupController = require('../controllers/roadPickup');
var AdminController = require('../controllers/admin');
var TripController = require('../controllers/trip');
app.use(cors())
router.use(cors())



//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


router.post('/finddriverforpassenger', TripController.findDriverForPassenger);
router.post('/acceptlivetrip', TripController.driverAcceptLiveTrip);
router.post('/endlivetrip', TripController.endLiveTrip);
router.post('/gettripdetailsbyid', TripController.getTripDetailsbyId);
router.post('/cancelbypassenger', TripController.cancelTripByPassenger);
router.post('/cancelbydriver', TripController.cancelTrip);

module.exports = router;
