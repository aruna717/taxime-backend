'use strict'

var express = require('express');
var router = express.Router();
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors')
var AdminController = require('../controllers/admin');
var auth = require('../services/authorize');

app.use(cors())
router.use(cors())

//support on x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.get('/', AdminController.admin);
router.get('/getdriverstoapprove', AdminController.adminloginRequired, AdminController.getDriversToApprove);
router.get('/getdrivers', auth.authorize(['super', 'operation']), AdminController.getDrivers);
router.post('/adminregister', AdminController.registerAdmin);
router.post('/adminlogin', AdminController.signInAdmin);
router.post('/adddriver', AdminController.adminloginRequired, AdminController.addDriver);
router.post('/approvedriver', AdminController.adminloginRequired, AdminController.adminApproveDriver);
router.post('/adduser', AdminController.adminloginRequired, AdminController.addUser);


router.get('/getAllUsers', AdminController.getAllUsers);
router.get('/getVehicleTracking', AdminController.getVehicleTracking);
router.post('/getOnlneDriversByRadious', AdminController.getOnlneDriversByRadious);
router.get('/cwallet', AdminController.createCompanyWallet);

router.delete('/cleartrackings', AdminController.clearVehicleTracks);

router.get('/getAllManualCustomers/:text', AdminController.getAllManualCustomers);

router.post('/getAllUserspagination', AdminController.getAllUserspagination);
router.post('/getAllManualCustomerspagination', AdminController.getAllManualCustomerspagination);

router.get('/getDashboardData/:from/:to', AdminController.getDashboardData);

router.get('/getCompanyWallet/:from/:to', AdminController.getCompanyWallet);
router.get('/getCompanyWalletpagination/:from/:to/:param/:text/:pageNo', AdminController.getCompanyWalletpagination);

router.get('/gettripDataByTripId/:id/:transactionType', AdminController.gettripDataByTripId);

router.get('/changeAndroidDriverVersion/:version', AdminController.changeAndroidDriverVersion);
router.get('/changeAndroidPassengerVersion/:version', AdminController.changeAndroidPassengerVersion);
router.get('/changeIosPassengerVersion/:version', AdminController.changeIosPassengerVersion);

module.exports = router;
